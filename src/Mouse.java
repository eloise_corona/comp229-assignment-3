import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/*MOVED MOUSE INTO ITS OWN SINGLETON CLASS SO IT ISN'T CONSTANTLY PASSED AROUND*/
public class Mouse implements MouseListener{
	private static Mouse singleton = null;
    private Point lastSeenMousePos;
	
	public boolean left;
	public boolean right;
	public Point position;
    public long stillMouseTime;
	
	public static Mouse getInstance() 
    { 
        if (singleton == null) 
        	singleton = new Mouse(); 
        return singleton; 
    } 
	
	public void updateMousePos(Point mousePos) {
		if(mousePos != null) {
			position = mousePos;
			if (lastSeenMousePos != null && lastSeenMousePos.equals(mousePos)) {
	            stillMouseTime++;
	        } else {
	            stillMouseTime = 0;
	        }
	    	lastSeenMousePos = mousePos;
		}
	}
		
	@Override
	public void mouseClicked(MouseEvent event) {
				
	}

	@Override
	public void mouseEntered(MouseEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent event) {
		switch(event.getButton()) {
		case MouseEvent.BUTTON1:{left = true;break;}
		case MouseEvent.BUTTON2:{right = true;break;}
		}

	}

	@Override
	public void mouseReleased(MouseEvent event) {
		switch(event.getButton()) {
		case MouseEvent.BUTTON1:{left = false;break;}
		case MouseEvent.BUTTON2:{right = false;break;}
		}		
	}
}
