import bos.*;

import java.awt.Color;
import java.awt.Graphics;
import java.util.*;

public class RabbitAdapter extends Character {
    private Rabbit adaptee;
    private Thread tempThread;
    public RabbitAdapter(Cell location) {
        super(location, null);
        display = Optional.of(Color.GRAY);
        adaptee = new Rabbit();
        description = "rabbit";
    }
    
    @Override
    public void setAiPath() { //sets path for the rabbit
        /**/
    	if(tempThread == null || tempThread.getState() == Thread.State.TERMINATED) { //creates thread
    		tempThread = new Thread(() -> {
    			moves = aiMoves();
    			moves.get(0).perform();
    		});
    		tempThread.start();
    	}
    }
    
    @Override
    public void moveOnPath() {
    }
    
    @Override
    public void paintPath(Graphics g) //draws rabbit's path
    {
    	if (selected){
            Cell originalLoc = this.getLocationOf();
            if(moves == null) {
            	return;
            }
            if(isMouseOver() && Mouse.getInstance().left) {
	            System.out.print("possible path:"); 
	            showPath(moves);
            }
            //assuming path is correct
            for(RelativeMove m: moves) {
                m.perform();
                this.getLocationOf().paintOverlay(g);
            }
            this.setLocationOf(originalLoc);
        }
    }

    @Override
    public List<RelativeMove> aiMoves(){
        switch (adaptee.nextMove()){
            case 0:
                return Arrays.asList(new MoveDown(Stage.getInstance().grid, this));
            case 1:
                return Arrays.asList(new MoveUp(Stage.getInstance().grid, this));
            case 2:
                return Arrays.asList(new MoveLeft(Stage.getInstance().grid, this));
            case 3:
                return Arrays.asList(new MoveRight(Stage.getInstance().grid, this));
        }
        return Arrays.asList(new MoveRandomly(Stage.getInstance().grid, this));
    }
}
