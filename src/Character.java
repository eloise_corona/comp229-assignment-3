import java.awt.*;
import java.util.Optional;
import java.util.List;

import bos.GamePiece;
import bos.MoveDown;
import bos.MoveLeft;
import bos.MoveRight;
import bos.MoveUp;
import bos.RelativeMove;

public abstract class Character implements GamePiece<Cell> {
    Optional<Color> display;
    Cell location;
    Behaviour behaviour;
    String description;

    protected boolean selected;
    protected List<RelativeMove> moves;

    public Character(Cell location, Behaviour behaviour){
        this.location = location;
        this.display = Optional.empty();
        this.behaviour = behaviour;
        this.description = "";
        this.selected = false;
    }
    
    public void update() { //to see if mouse is over the character
    	if(Mouse.getInstance().left) {
    		selected = isMouseOver();
    	}
    }

    public void paint(Graphics g){
        if(display.isPresent()) {
            g.setColor(display.get());
            g.fillOval(location.x + location.width / 4, location.y + location.height / 4, location.width / 2, location.height / 2);
            g.setColor(Color.BLACK);
            g.drawOval(location.x + location.width/4, location.y + location.height/4, location.width/2, location.height/2);
        }
        paintPath(g); //draw the path if the mouse left clicks a character
        boolean isHovered = isMouseOver() && Mouse.getInstance().stillMouseTime > 20;
        if(isHovered || selected) {
        	int width = g.getFontMetrics().stringWidth(description);
        	int startY = location.y - 15 - (isMouseOver() && isHovered? 1: 0) * 15;
        	
        	g.setColor(display.orElse(Color.WHITE));
            g.fillRoundRect(location.x + 20, startY, width + 10, 15, 3, 3);
            g.setColor(Color.BLACK);
            g.drawString(description, location.x + 25, startY + 12);
        }
    }
    
    public void setAiPath() {
    	moves = aiMoves();
    }
    
    public void moveOnPath() {
		moves.get(0).perform();
    }

    public void setLocationOf(Cell loc){
        this.location = loc;
    }

    public Cell getLocationOf(){
        return this.location;
    }

    public void setBehaviour(Behaviour behaviour){
        this.behaviour = behaviour;
    }

    public List<RelativeMove> aiMoves(){
        return behaviour.chooseMoves(this);
    }
    
    public boolean isMouseOver()
    {
    	return (this.location.isHighlighted());
    }

    public void paintPath(Graphics g) //to draw in the path
    {
    	if (selected){
            Cell originalLoc = this.getLocationOf();
            if(moves == null) {
            	moves = this.aiMoves();
            }
            if(isMouseOver() && Mouse.getInstance().left) {
	            System.out.print("possible path:"); 
	            showPath(moves);
            }
            //assuming that the path is correct
            for(RelativeMove m: moves) {
                m.perform();
                this.getLocationOf().paintOverlay(g);
            }
            this.setLocationOf(originalLoc);
        }
    }

    /*moved from stage*/
    public static void showPath(List<RelativeMove> lst){
        lst.forEach(rm -> {
            if (rm instanceof MoveUp){
                System.out.print("^");
            } else if (rm instanceof MoveDown){
                System.out.print("\\/");
            } else if (rm instanceof MoveRight){
                System.out.print("->");
            } else if (rm instanceof MoveLeft) {
                System.out.print("<-");
            }
        });
        System.out.println("");
    }
}
