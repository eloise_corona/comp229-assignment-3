import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.*;
import java.time.Duration;
import java.time.Instant;

public class Main extends JFrame implements Runnable {
	private Canvas canvas;
	
    private class Canvas extends JPanel implements KeyListener {

        private Stage stage;

        public Canvas() {
            setPreferredSize(new Dimension(1280, 720));
            stage = Stage.getInstance();
            addMouseListener(Mouse.getInstance()); //adds a mouse listener for mouse actions
        }

        @Override
        public void paint(Graphics g) {
            stage.paint(g);
        }

        @Override
        public void keyTyped(KeyEvent e) {
        	stage.handleKeyboard(e);
        }

        @Override
        public void keyPressed(KeyEvent e) {}

        @Override
        public void keyReleased(KeyEvent e) {}
    }

    public static void main(String[] args) {
        Main window = new Main();
        window.run();
    }

    private Main() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        canvas = new Canvas();
        this.setContentPane(canvas);
        this.addKeyListener(canvas);
        this.pack();
        this.setVisible(true);
    }
    

    @Override
    public void run() {
        while (true) {
            Stage.getInstance().handleMouse(canvas.getMousePosition());
            Stage.getInstance().update();
            this.repaint();
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                System.out.println("thread was interrupted, but really, who cares?");
                System.out.println(e.toString());
            }
        }
    }
}
