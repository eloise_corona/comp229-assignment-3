import bos.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Grid implements GameBoard<Cell> {

    static int gridSize = 35;
    static int offset = 10; //to move the grid away from the border
    private Cell[][] cells; //initialise instance of cell but do not give size yet

    private int width; //width variable
    private int height; //height variable

    public Grid(int gridWidth, int gridHeight) { //constructor for height and width
    	this.width = gridWidth;
        this.height = gridHeight;
        
        
        cells = new Cell[height][width]; //adds height and width for cell

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                cells[j][i] = new Cell(width + i * gridSize + offset, height + j * gridSize + offset, gridSize);
            }
        }
    }

    public void blockCell(int row, int col){
        cells[row][col].traversable = false;
    }
    
    public void update() { //calls each cell's update
    	doToEachCell((c) -> {
    		c.update();
    	});
    }

    public void paint(Graphics g) { //paint each cell
        doToEachCell((c) -> {
            c.paint(g);
        });
    }

    public Cell getRandomCell() {
        java.util.Random rand = new java.util.Random();
        return cells[rand.nextInt(height)][rand.nextInt(width)];
    }

    private bos.Pair<Integer, Integer> indexOfCell(Cell c) {
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (cells[y][x] == c) {
                    return new bos.Pair(y, x);
                }
            }
        }
        return null;
    }

    public Pair<Integer, Integer> findAmongstCells(Predicate<Cell> predicate) {
    	for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (predicate.test(cells[y][x]))
                    return new Pair(y, x);
            }
        }
        return null;
    }

    public Optional<Pair<Integer, Integer>> safeFindAmongstCells(Predicate<Cell> predicate) {
    	for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if (predicate.test(cells[y][x]))
                    return Optional.of(new Pair(y, x));
            }
        }
        return Optional.empty();

    }

    private void doToEachCell(Consumer<Cell> func) {
    	for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                func.accept(cells[y][x]);
            }
        }
    }

    @Override
    public Optional<Cell> below(Cell relativeTo) {
        return safeFindAmongstCells((c) -> c == relativeTo)
                .filter((pair) -> pair.first < height - 1)
                .map((pair) -> cells[pair.first + 1][pair.second]);
    }

    @Override
    public Optional<Cell> above(Cell relativeTo) {
        return safeFindAmongstCells((c) -> c == relativeTo)
                .filter((pair) -> pair.first > 0)
                .map((pair) -> cells[pair.first - 1][pair.second]);
    }

    @Override
    public Optional<Cell> rightOf(Cell relativeTo) {
        return safeFindAmongstCells((c) -> c == relativeTo)
                .filter((pair) -> pair.second < width - 1)
                .map((pair) -> cells[pair.first][pair.second + 1]);
    }

    @Override
    public Optional<Cell> leftOf(Cell relativeTo) {
        return safeFindAmongstCells((c) -> c == relativeTo)
                .filter((pair) -> pair.second > 0)
                .map((pair) -> cells[pair.first][pair.second - 1]);
    }

    public Cell cellAtRowCol(Integer row, Integer col) {
        return cells[row][col];
    }


    @Override
    public List<RelativeMove> movesBetween(Cell from, Cell to, GamePiece<Cell> mover) {
        System.out.println(((Character)mover).description + ": calculating a moves between: " + from + " -> " + to);
        class Node extends bos.Pair<Cell, List<RelativeMove>>{public Node(Cell a, List<RelativeMove> b){super(a, b);}} // first cell is the node, second is the cell that led to it

        // clear all ticks from cells
        doToEachCell(c -> c.ticked = false);
        java.util.Queue<Node> frontier = new java.util.LinkedList<Node>();

        frontier.add(new Node(from, new java.util.ArrayList()));

        Node curr = frontier.remove();
        while(curr.first != to){
            Optional<Cell> lo = leftOf(curr.first);
            if (lo.isPresent() && !lo.get().ticked && lo.get().traversable){
                lo.get().ticked = true;
                List<RelativeMove> sofar = new ArrayList(curr.second);
                sofar.add(new MoveLeft(this, mover));
                frontier.add(new Node(lo.get(), sofar));
            }
            Optional<Cell> ro = rightOf(curr.first);
            if (ro.isPresent() && !ro.get().ticked && ro.get().traversable){
                ro.get().ticked = true;
                List<RelativeMove> sofar = new ArrayList(curr.second);
                sofar.add(new MoveRight(this, mover));
                frontier.add(new Node(ro.get(), sofar));
            }
            Optional<Cell> bo = below(curr.first);
            if (bo.isPresent() && !bo.get().ticked && bo.get().traversable){
                bo.get().ticked = true;
                List<RelativeMove> sofar = new ArrayList(curr.second);
                sofar.add(new MoveDown(this, mover));
                frontier.add(new Node(bo.get(), sofar));
            }
            Optional<Cell> ao = above(curr.first);
            if (ao.isPresent()&& !ao.get().ticked && ao.get().traversable){
                ao.get().ticked = true;
                List<RelativeMove> sofar = new ArrayList(curr.second);
                sofar.add(new MoveUp(this, mover));
                frontier.add(new Node(ao.get(), sofar));
            }

            if(frontier.size() > 0)
                curr = frontier.remove();
            else
                break;
        }

        return curr.second;
    }
}