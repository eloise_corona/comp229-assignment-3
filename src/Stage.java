import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.*;
import java.time.*;
import java.util.List;

import bos.Pair; //to figure out size of grid

public class Stage extends KeyObservable {
    protected Grid grid;
    protected Character sheep;
    protected Character shepherd;
    protected Character wolf;
    protected RabbitAdapter rabbit;
    private List<Character> allCharacters;
    protected Player player;

    private Momento quickSave; //memento object moved into stage

    private Instant timeOfLastMove = Instant.now();

    private static Stage instance = new Stage();

    /*sets initial starting points for each character*/
    public static Stage getInstance(){
        return instance;
    }

    private Stage() {
        SAWReader sr = new SAWReader("data/stage1.saw");
        grid     = new Grid(20, 20);
        shepherd = new Shepherd (grid.cellAtRowCol(sr.getShepherdLoc().first, sr.getShepherdLoc().second), new StandStill());
        sheep    = new Sheep	(grid.cellAtRowCol(sr.getSheepLoc().first	, sr.getSheepLoc().second)	 , new MoveTowards(shepherd));
        wolf     = new Wolf		(grid.cellAtRowCol(sr.getWolfLoc().first	, sr.getWolfLoc().second)	 , new MoveTowards(sheep));
        rabbit 	 = new RabbitAdapter(grid.getRandomCell());

        player = new Player(grid.cellAtRowCol(sr.getPlayerLoc().first,sr.getPlayerLoc().second)); //gets the coords of the player
        this.register(player);
        player.startMove();

        // put in the blocks we found in the config file
        sr.getBlocks().forEach(p -> grid.blockCell(p.first, p.second));

        /*adds characters onto stage*/
        allCharacters = new ArrayList<Character>();
        allCharacters.add(sheep); //separated add statement for neatness
        allCharacters.add(shepherd);
        allCharacters.add(wolf);
        allCharacters.add(rabbit);
    }

    /*to figure out the size of the game*/
    public Point findGridSize(SAWReader sr) {
        Pair<Integer,Integer> highestSize = new Pair<Integer,Integer>(10, 10); //minimum size is set to 10x10
        highestSize = compare(sr.getSheepLoc(),highestSize);
        highestSize = compare(sr.getShepherdLoc(),highestSize);
        highestSize = compare(sr.getPlayerLoc(),highestSize);
        highestSize = compare(sr.getWolfLoc(),highestSize);

        /*loop the list of blocks*/
        for (Pair<Integer, Integer> block : sr.getBlocks()) {
            highestSize = compare(block,highestSize);
        }

        return new Point(highestSize.second +1,highestSize.first +1);
    }

    /*gets pair of height and width for grideSize to find size of the game*/
    private Pair<Integer,Integer> compare(Pair<Integer,Integer> left, Pair<Integer,Integer> right) {
        Pair<Integer,Integer> tempPair = new Pair<Integer,Integer>(0, 0);
        if(left.first > right.first) {
            tempPair.first = left.first;
        } else {
            tempPair.first = right.first;
        }
        if(left.second > right.second) {
            tempPair.second = left.second;
        } else {
            tempPair.second = right.second;
        }
        return tempPair;
    }

    /*to save and restore momento*/
    public void handleKeyboard(KeyEvent e) {
    	if (e.getKeyChar() == ' ') {
            quickSave = getMomento();
        } else if(e.getKeyChar() == 'r'){
            restoreMomento(quickSave);
        } else {
            this.notifyAll(e.getKeyChar(), this.grid);
        }
	}

    /*gets mouse position*/
    public void handleMouse(Point mousePos) {
    	Mouse.getInstance().updateMousePos(mousePos);
    }

    public void update(){
    	grid.update(); //updating cells and checking if mouse is over them
    	allCharacters.forEach((c) ->{ //same thing as above but when characters are clicked, to keep path highlighted
    		c.update();
    	});
    	/*checks if sheep is safe or is dead*/
        if (!player.inMove()) {
            if (sheep.location == shepherd.location) {
                System.out.println("The sheep is safe :)");
                System.exit(0);
            } else if (sheep.location == wolf.location) {
                System.out.println("The sheep is dead :(");
                System.exit(1);
            } else { //begins player move, updates the characters to their new position, and updates the path
            	player.startMove();
            	timeOfLastMove = Instant.now();
            	updateCharacters();
            	allCharacters.forEach((c) -> {
                	c.setAiPath();
            	});
            }
        }
    }
        
    private void updateCharacters() { //to set the new path of characters
    	allCharacters.forEach((c) -> {
        	c.setAiPath();
    	});
    	
    	allCharacters.forEach((c)->{ //to move the characters and set new path
    		c.setAiPath();
    		c.moveOnPath();
    	});
    }

    public void paint(Graphics g) {
        grid.paint(g);
        sheep.paint(g);
        shepherd.paint(g);
        wolf.paint(g);
        rabbit.paint(g);
        player.paint(g);
    }

    // momento implmentation
    public static class Momento{
        private Cell wolfLoc;
        private Cell shepLoc;
        private Cell sheepLoc;
        private Cell rabbitLoc;
        private Cell playerLoc;
        private Behaviour wolfBeh;
        private Behaviour shepBeh;
        private Behaviour sheepBeh;
        public Momento(Stage s){
            wolfLoc = s.wolf.location;
            shepLoc = s.shepherd.location;
            sheepLoc = s.sheep.location;
            rabbitLoc = s.rabbit.location;
            playerLoc = s.player.location;
            wolfBeh = s.wolf.behaviour;
            shepBeh = s.shepherd.behaviour;
            sheepBeh = s.sheep.behaviour;
        }
    }

    public Momento getMomento(){
        return new Momento(this);
    }

    public void restoreMomento(Momento m){
        wolf.location = m.wolfLoc;
        sheep.location = m.sheepLoc;
        shepherd.location = m.shepLoc;
        player.location = m.playerLoc;
        wolf.behaviour = m.wolfBeh;
        shepherd.behaviour = m.sheepBeh;
        sheep.behaviour = m.sheepBeh;
    }
}